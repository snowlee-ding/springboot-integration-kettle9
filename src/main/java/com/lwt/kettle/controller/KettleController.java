package com.lwt.kettle.controller;

import com.lwt.kettle.common.KETTLE;
import com.lwt.kettle.common.config.kettle.KettleServer;
import com.lwt.kettle.common.utils.ResultDTO;
import com.lwt.kettle.common.utils.BaseController;
import com.lwt.kettle.common.utils.GsonUtils;
import com.lwt.kettle.UrlMapping;
import org.pentaho.di.core.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class KettleController extends BaseController {
    private static final Logger log = LoggerFactory.getLogger(KettleController.class);

    @Autowired
    private KettleServer kettleServer;


    @RequestMapping(value = UrlMapping.KETTLE_TESTJOB, method = RequestMethod.POST)
    public ResultDTO<Boolean> testKettleJob(@RequestBody Map<String, String> map) {
        long startMillis = System.currentTimeMillis();
        Result kettleRes = null;
        try {
            kettleRes = kettleServer.executeJob(KETTLE.JOB_ORG, map);
            long endMillis = System.currentTimeMillis();
            log.info("KETTLE执行完毕,耗时:{}毫秒", endMillis - startMillis);
            if (!kettleServer.isSuccess(kettleRes)) {
                log.info("执行作业发生异常 -> {}", GsonUtils.Bean2Json(kettleRes));
                return ResultDTO.err(GsonUtils.Bean2Json(kettleRes));
            }
            log.info("执行作业成功 -> {}", GsonUtils.Bean2Json(kettleRes));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            log.info("kettle执行日志 ↓↓↓↓↓↓↓↓↓↓↓↓ \n {}", kettleRes.getLogText());
        }
        return success(Boolean.TRUE);
    }


    @RequestMapping(value = UrlMapping.KETTLE_TESTPLAN, method = RequestMethod.POST)
    public ResultDTO<Boolean> testKettlePlan(@RequestBody Map<String, String> map) {
        long startMillis = System.currentTimeMillis();
        Result kettleRes = null;
        try {
            kettleRes = kettleServer.executePlan("example4-单表的全量同步.ktr", map);
            long endMillis = System.currentTimeMillis();
            log.info("KETTLE执行完毕,耗时:{}毫秒", endMillis - startMillis);

            if (!kettleServer.isSuccess(kettleRes)) {
                log.info("执行作业发生异常 -> {}", GsonUtils.Bean2Json(kettleRes));
                return ResultDTO.err(GsonUtils.Bean2Json(kettleRes));
            }
            log.info("执行作业成功 -> {}", GsonUtils.Bean2Json(kettleRes));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //log.info("kettle执行日志 ↓↓↓↓↓↓↓↓↓↓↓↓ \n {}", kettleRes.getLogText());
            log.info("kettle执行日志 ↓↓↓↓↓↓↓↓↓↓↓↓ \n {}", kettleRes);
        }
        return success(Boolean.TRUE);
    }

    @RequestMapping(value = UrlMapping.TEST, method = RequestMethod.GET)
    public ResultDTO<Boolean> test() {
        return success(Boolean.TRUE);
    }

}
