package com.lwt.kettle;

public interface UrlMapping {

    /**
     * 执行kettle作业
     */
    String KETTLE_TESTJOB = "/kettle/testJob";

    /**
     * 执行kettle转换
     */
    String KETTLE_TESTPLAN = "/kettle/testPlan";


    String TEST="/test";

}
