package com.lwt.kettle;

import com.lwt.kettle.common.config.kettle.KettleConfig;
import com.lwt.kettle.common.config.kettle.KettleServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class KettleRun {

    public static void main(String[] args) {
        SpringApplication.run(KettleRun.class, args);
    }


    @Bean
    public KettleServer initKettleServer(){
        return new KettleServer();
    }

    @Bean
    @ConfigurationProperties(prefix = "kettle")
    public KettleConfig initKettleConfig(){
        return new KettleConfig();
    }
}
