package com.lwt.kettle.common.config;


import com.lwt.kettle.common.config.kettle.KettleConfig;
import com.lwt.kettle.common.config.kettle.KettleServer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommonConfig {

    /**
     * Kettle配置文件获取
     *
     * @return
     */
    @Bean
    @ConfigurationProperties(prefix = "kettle")
    public KettleConfig initKettleConfig() {
        return new KettleConfig();
    }

    /**
     * kettle配置
     *
     * @return
     */
    @Bean
    public KettleServer initKettleServer() {
        return new KettleServer();
    }

}
