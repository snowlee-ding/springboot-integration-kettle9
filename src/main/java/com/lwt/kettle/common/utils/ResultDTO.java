package com.lwt.kettle.common.utils;


public class ResultDTO<T> {

    /**
     * 是否成功
     */
    private Boolean success;

    /**
     * 返回描述
     */
    private String msg;

    /**
     * API 状态码
     */
    private String code;

    /**
     * API数据
     */
    private T data;

    /**
     * 页码
     */
    private Integer pageNum;

    /**
     * 每页条数
     */
    private Integer pageSize;

    /**
     * 总页数
     */
    private Integer pageCount;

    /**
     * 总条数
     */
    private Long total;


    public ResultDTO() {
        this.success = Boolean.TRUE;
        this.code = "OK";
        this.msg = "OK";
    }

    public ResultDTO(T data) {
        this.success = Boolean.TRUE;
        this.code = "OK";
        this.msg = "OK";
        this.data = data;
    }

    private ResultDTO(T data, String code) {
        this.success = Boolean.TRUE;
        this.code = code;
        this.data = data;
    }

    private ResultDTO(T data, String code, String msg) {
        this.success = Boolean.TRUE;
        this.msg = msg;
        this.code = code;
        this.data = data;
    }

    public static <T> ResultDTO<T> newResult() {
        return empty();
    }

    public static <T> ResultDTO<T> newResult(T data) {
        return ok(data);
    }

    public static <T> ResultDTO<T> err(String code, String msg) {
        ResultDTO<T> inst = new ResultDTO();
        inst.code = code;
        inst.msg = msg;
        inst.success = Boolean.FALSE;
        return inst;
    }

    public static <T> ResultDTO<T> err(T data, String code, String msg) {
        ResultDTO<T> inst = new ResultDTO();
        inst.code = code;
        inst.msg = msg;
        inst.data = data;
        inst.success = Boolean.FALSE;
        return inst;
    }

    public static <T> ResultDTO<T> err(String msg) {
        return err("RY00", msg);
    }

    public static <T> ResultDTO<T> err(Exception e) {
        ResultDTO<T> inst = new ResultDTO();
        inst.code = "500";
        inst.msg = e.getMessage();
        inst.success = false;
        return inst;
    }

    public static <T> ResultDTO<T> ok(T data) {
        return new ResultDTO(data);
    }

    public static <T> ResultDTO<T> ok(T data, String code) {
        return new ResultDTO(data, code);
    }

    public static <T> ResultDTO<T> ok(T data, String code, String msg) {
        return new ResultDTO(data, code, msg);
    }


    public static <T> ResultDTO<T> empty() {
        return new ResultDTO();
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(Integer pageCount) {
        this.pageCount = pageCount;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
