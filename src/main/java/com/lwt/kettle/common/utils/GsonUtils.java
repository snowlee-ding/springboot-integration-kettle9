package com.lwt.kettle.common.utils;


import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GsonUtils {

    public static final String TAG = GsonUtils.class.getSimpleName();
    private final static Logger Log = LoggerFactory.getLogger(GsonUtils.class);
    private static final String DATE_FORMAT_DEFAULT = "yyyy-MM-dd HH:mm:ss";
    private static Gson gson = null;

    /**
     * 判断是否存在，不存在则创建
     */
    static {
        if (gson == null) {
            // gson = new Gson();
            // 当使用 GsonBuilder 方式时属性为空的时候输出来的json字符串是有键值key的,显示形式是"key":null，而直接 new 出来的就没有"key":null的
            gson = buildGson();
        }
    }

    private GsonUtils() {
    }

    /**
     * 默认的 GSON 初始化
     */
    public static Gson buildGson() {
        return new Gson().newBuilder().setDateFormat(DATE_FORMAT_DEFAULT).create();
    }

    /**
     * 将对象转成json格式
     * Bean To Json
     *
     * @param object
     * @return String
     */
    public static String Bean2Json(Object object) {
        String jsonString = null;
        try {
            if (gson != null) {
                jsonString = gson.toJson(object);
            }
        } catch (Exception e) {
            Log.error(TAG, "Bean 转 Json 格式异常:" + e);
        }
        return jsonString;
    }

    /**
     * 将 json 转成特定的 cls 的对象
     * Json To Bean
     *
     * @param jsonString
     * @param cls
     * @return
     */
    public static <T> T Json2Bean(String jsonString, Class cls) {
        T t = null;
        try {
            if (gson != null) {
                // 传入json对象和对象类型,将json转成对象
                t = (T) gson.fromJson(jsonString, cls);
            }
        } catch (Exception e) {
            Log.error(TAG, "Json 转 Bean 非法json字符串:" + e);
        }
        return t;
    }

    /**
     * json字符串转成list
     * 解决泛型问题
     * 备注：
     * List list=gson.fromJson(jsonString, new TypeToken<List>() {}.getType());
     * 该方法会报泛型类型擦除问题
     *
     * @param jsonArray
     * @param cls
     * @param
     * @return
     */
    public static List Json2List(String jsonArray, Class cls) {
        List list = new ArrayList();
        try {
            if (gson != null) {
                JsonArray array = new JsonParser().parse(jsonArray).getAsJsonArray();
                for (final JsonElement elem : array) {
                    list.add(gson.fromJson(elem, cls));
                }
            }
        } catch (JsonSyntaxException e) {
            Log.error(TAG, "Json 转 List 非法json字符串:" + e);
        }
        return list;
    }

    /**
     * json 字符串转成 list map
     * Json To List<Map<String,T>>
     *
     * @param jsonObjectList
     * @return
     */
    public static <T> List<Map<String, T>> Json2ListMaps(String jsonObjectList) {
        List<Map<String, T>> list = null;
        try {
            if (gson != null) {
                list = gson.fromJson(jsonObjectList, new TypeToken<List<Map<String, T>>>() {
                }.getType());
            }
        } catch (JsonSyntaxException e) {
            Log.error(TAG, "Json 转 List 非法json字符串:" + e);
        }
        return list;
    }

    /**
     * json 字符串转成 map 的
     * Json To Map
     *
     * @param jsonObject
     * @return
     */
    public static <T> Map<String, T> Json2Maps(String jsonObject) {
        Map<String, T> map = null;
        try {
            if (gson != null) {
                map = gson.fromJson(jsonObject, new TypeToken<Map<String, T>>() {
                }.getType());
            }
        } catch (JsonSyntaxException e) {
            Log.error(TAG, "Json 转 Map 非法json字符串:" + e);
        }
        return map;
    }

}
