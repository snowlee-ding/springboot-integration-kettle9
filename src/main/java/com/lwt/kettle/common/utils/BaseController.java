package com.lwt.kettle.common.utils;

import com.github.pagehelper.Page;
import com.lwt.kettle.common.TConstants;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.Map;

public class BaseController {

    /**
     * 默认成功返回
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResultDTO<T> success(T data) {
        return ResultDTO.ok(data, TConstants.REST.OK.getCode(), TConstants.REST.OK.getMsg());
    }

    /**
     * 默认失败返回
     *
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResultDTO<T> err(T data) {
        return ResultDTO.err(data, TConstants.REST.ERR.getCode(), TConstants.REST.ERR.getMsg());
    }

    /**
     * 从查询的传入条件Map中获取分页对象pageInfo
     *
     * @param map 页面传出的查询条件
     * @return 分页参数对象
     */
    @SuppressWarnings("unchecked")
    public SimplePageInfo getPageInfoByMap(Map<String, Object> map) {
        Object obj = map.get("pageInfo");
        SimplePageInfo pageInfo = new SimplePageInfo();
        if (obj != null) {
            if (obj instanceof SimplePageInfo) {
                pageInfo = (SimplePageInfo) obj;
            } else {
                pageInfo = BeanConvertUtils.mapToBean((Map<String, Object>) obj, SimplePageInfo.class);
            }
            if (pageInfo.getPageSize() > TConstants.MAX_PAGE_SIZE) {
                pageInfo.setPageSize(TConstants.MAX_PAGE_SIZE);
            }
            map.remove("pageInfo");
        }
        return pageInfo;
    }

    public SimplePageInfo getPageInfoByPageInfo(SimplePageInfo page) {
        SimplePageInfo pageInfo = new SimplePageInfo();
        if (page != null) {
            if (pageInfo.getPageSize() > TConstants.MAX_PAGE_SIZE) {
                pageInfo.setPageSize(TConstants.MAX_PAGE_SIZE);
            }
            return page;
        }
        return pageInfo;
    }

    /**
     * 从分页查询结果 Page &lt; Map &lt; String, Object &gt; &gt; 中获取返回对象列表
     *
     * @param page  查询结果page对象
     * @param clazz 要返回前台的数据类型
     * @return 符合clazz标准的ResultDTO对象
     */
    public <T> ResultDTO<List<T>> getPageDataByMap(Page<Map<String, Object>> page, Class<T> clazz) {
        ResultDTO<List<T>> result = new ResultDTO<List<T>>();
        List<T> data = BeanConvertUtils.mapToBeanInList(page, clazz);
        result.setPageCount(page.getPages());
        result.setTotal(page.getTotal());
        result.setPageNum(page.getPageNum());
        result.setPageSize(page.getPageSize());
        result.setData(data);
        return result;
    }

    /**
     * 返回验证信息
     */
    public <T> ResultDTO<T> getErrorInfo(BindingResult result) {
        ResultDTO<T> resultDTO = new ResultDTO<T>();
        StringBuilder stringBuilder = new StringBuilder();
        List<ObjectError> list = result.getAllErrors();
        for (ObjectError error : list) {
            stringBuilder.append(error.getDefaultMessage()).append("\n");
        }
        resultDTO.setSuccess(false);
        resultDTO.setMsg(stringBuilder.toString());
        return resultDTO;
    }

    public <T> List<T> getListDataByMap(List<Map<String, Object>> page, Class<T> clazz) {
        List<T> data = BeanConvertUtils.mapToBeanInList(page, clazz);
        return data;
    }

}
