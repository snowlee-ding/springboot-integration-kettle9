package com.lwt.kettle.common;

/**
 * kettle文件定义
 */
public interface KETTLE {

    /********************************kettle 作业*************************/

    /**
     * 作业-org文件
     */
    public String JOB_ORG = "Org.kjb";


    /********************************kettle 转换*************************/

    /**
     * 转换-org表
     */
    public String TRAN_ORG = "orgDepartment.ktr";

}
