package com.lwt.kettle.common;

public class TConstants {

    /**
     * 分页中每页大小上限
     */
    public static final Integer MAX_PAGE_SIZE = 3000;

    public enum REST {
        OK("0", "请求成功"),
        ERR("503", "请求失败,请稍后重试"),
        ;

        private String code;
        private String msg;
        REST(String code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }

}
